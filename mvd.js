import { FTPClient } from "https://deno.land/x/ftpc@v1.2.1/mod.ts";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const ftpHost = Deno.env.get("FTP_HOST");
const user = Deno.env.get("FTP_USER");
const pass = Deno.env.get("FTP_PASS");
const articleFilepath = Deno.env.get("FTP_ARTICLES_FILEPATH");
const stockFilepath = Deno.env.get("FTP_STOCK_FILEPATH");

const client = new FTPClient(ftpHost, {
  user: user,
  pass: pass,
  mode: "passive",
  port: 21,
});

let articleMap = {};

await client.connect();
//await processCurrentSkus();
await processArticleFiles(client);
await processStockFiles(client);
for (let articleInMap of Object.values(articleMap)) {
  await sagalDispatch(articleInMap);
}
await client.close();

async function processArticleFiles(client) {
  let filepath = articleFilepath;
  try {
    let articles = await client.download(filepath);

    const decoder = new TextDecoder();
    let articleText = decoder.decode(articles).split("\n");

    for (let articleLine of articleText) {
      await processArticle(articleLine.split("|"));
    }
  } catch (e) {
    console.log(`Unable to download file: ${filepath}, message: ${e.message}`);
  }
}

async function processStockFiles(client) {
  let filepath = stockFilepath;
  try {
    let articleStocks = await client.download(filepath);

    const decoder = new TextDecoder();
    let articleStockText = decoder.decode(articleStocks).split("\n");

    for (let articleStockLine of articleStockText) {
      await processArticleStock(articleStockLine.split("|"));
    }
  } catch (e) {
    console.log(`Unable to download file: ${filepath}, message: ${e.message}`);
  }
}

async function processArticle(article) {
  let articleSku = article[0];
  let articleName = article[1];
  let articlePrice = {
    value: 0,
    currency: "U$S",
  };
  if (article[15]) {
    articlePrice.value = Number(article[15].replace(",", "."));
    articlePrice.currency = "$";
  } else if (article[16]) {
    articlePrice.value = Number(article[16].replace(",", "."));
  }

  let processedArticle = {
    sku: articleSku,
    client_id: clientId,
    merge: false,
    integration_id: clientIntegrationId,
    ecommerce: Object.values(clientEcommerce).map((ecommerce_id) => {
      return {
        ecommerce_id: ecommerce_id,
        properties: [
          { name: articleName },
          {
            price: articlePrice,
          },
          { stock: 0 },
        ],
        variants: [],
      };
    }),
  };

  articleMap[articleSku] = processedArticle;
}

async function processArticleStock(articleStock) {
  let articleSku = articleStock[0];

  if (articleMap[articleSku]) {
    if (articleStock[2]) {
      for (let articleInEcommerce of articleMap[articleSku].ecommerce) {
        articleInEcommerce.properties[2] = {
          stock: Number(articleStock[2].replace(",", ".")),
        };
      }
    }
  }
}
