### Integration script for client: MVD Sports

## Introduction

This script was created to run in the generic integration platform that links Sagal's clients' various forms of sending data into payloads that the business platform can understand. MVD Sports is a client that mainly hosts their data in a FTP Server that we can access to retrieve data.

## Workflow

- Article data is retrieved from the client's FTP server.
- The data is restructured and processed for each article, being transformed to a Sagal-friendly format.
- Stock data is retrieved from the client's FTP server.
- The stock data is added to the existing article data where applicable.
- The restructured data is dispatched to Sagal.

## Environment Variables

- **CLIENT_ID**: Sagal's known identifier for the client. A number.
- **CLIENT_INTEGRATION_ID**: the integration script's id in the existing runtime environment. A number.
- **CLIENT_ECOMMERCE**: a map holding this client's known identifiers for each E-Commerce service. (e.g: Key MELI for Mercadolibre)
- **FTP_HOST**: The client's FTP server address.
- **FTP_USER**: The username required to log into the client's FTP server.
- **FTP_PASSWORD**: The password required to log into the client's FTP server.
- **FTP_ARTICLES_FILEPATH**: The filepath for the file holding the client's articles.
- **FTP_STOCK_FILEPATH**: The filepath for the file holding the client's article stock data.

## Functions

### processArticleFiles(FTPClient client)

**Description:** processes the given article file by calling the processArticle function for every existing article retrieved after downloading the file.

### processArticle(Object article)

**Description:** Reestructures the article data according to Sagal's requirements, loading the processed article into an article map where articles can be identified by their sku in short order.

### processStockFiles(FTPClient client)

**Description:** processes the given article stock file by calling the processArticleStock function for every existing stock data retrieved after downloading the file.

### processArticleStock(Object articleStock)

**Description:** Assigns the registered stock amounts to matching articles in memory.
